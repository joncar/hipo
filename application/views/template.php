<!doctype html>
<html lang="en" class="no-js">
    <head>
        <meta charset="UTF-8">
        <title><?= !empty($title)?$title:'HIPO' ?></title>
        <meta name="description" content="És un estudi creatiu de disseny gràfic especialitzat en identitat corporativa, disseny editorial, packaging, disseny i desenvolupament de webs i apps. " >
        <meta name="author" content="Jordi Magaña . HIPO.">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Import Themify icon Style -->
        <link href="<?= base_url() ?>css/template/themify-icons.css" rel="stylesheet">
        <link rel="stylesheet" href="<?= base_url() ?>css/template/style.css"> <!-- Resource style -->
        <link rel="stylesheet" href="<?= base_url() ?>css/template/headlines.css"> <!-- Resource style -->
        <link rel="stylesheet" href="<?= base_url() ?>css/template/menu.css">
        <link rel="stylesheet" href="<?= base_url() ?>css/template/animsition.min.css">
        <link rel="stylesheet" href="<?= base_url() ?>css/template/responsive.css">
        <link rel="stylesheet" href="<?= base_url() ?>css/template/awwwards.css">
        <!--[if lt IE 8]><!-->
        <link rel="stylesheet" href="<?= base_url() ?>css/template/ie7/ie7.css">
        <!--<![endif]-->
        <!--- Favicons -->
        <link rel="shortcut icon" href="favicon.png">
        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <link rel="apple-touch-icon" sizes="72x72" href="apple-touch-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="114x114" href="apple-touch-icon-114x114.png">
        <!--[if IE]>
                    <script src="http://html5shiv-printshiv.googlecode.com/svn/trunk/html5shiv-printshiv.js"></script>

                <![endif]-->
        <link href="//fonts.googleapis.com/css?family=Montserrat&subset=latin" rel="stylesheet" type="text/css">
        <style>
           *{
            font-family: 'Montserrat', sans-serif;
           }
        </style>
                <!-- Include modernizr-2.8.3.min.js -->
        <script src="<?= base_url() ?>js/template/modernizr-2.8.3.min.js"></script>
        <!-- Include jquery.min.js plugin -->
        <script src="<?= base_url() ?>js/template/jquery-2.1.3.min.js"></script>
    </head>
    <body class="homepage">
        <?php $this->load->view($view); ?>      
        <?php if(empty($scripts)){$this->load->view('includes/template/scripts');} ?>      
    </body>
</html>

