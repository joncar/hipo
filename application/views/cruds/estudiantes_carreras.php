<?= $output ?>
<script>
	$(document).ready(function() {
        $(document).on('change','#field-sedes_id, #field-modalidades_id',function(e) {
            if($(this).val()!=''){
	            e.stopPropagation();
	            var selectedValue = $('#field-sedes_id').val();					
	            $.post('ajax_extension/programacion_carreras_id/sedes_id/'+encodeURI(selectedValue.replace(/\//g,'_agsl_')), {sedes_id:selectedValue,modalidades_id:$("#field-modalidades_id").val()}, function(data) {					
	            var $el = $('#field-programacion_carreras_id');
	                      var newOptions = data;
	                      $el.empty(); // remove old options
	                      $el.append($('<option></option>').attr('value', '').text(''));
	                      $.each(newOptions, function(key, value) {
	                        $el.append($('<option></option>')
	                           .attr('value', key).text(value.replace(/&(nbsp|amp|quot|lt|gt);/g,' ')));
	                        });
	                      //$el.attr('selectedIndex', '-1');
	                      $el.chosen().trigger('liszt:updated');

            },'json');
            $('#field-programacion_carreras_id').change();
        }
        });
});
</script>