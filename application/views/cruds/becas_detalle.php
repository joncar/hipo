<?= $output ?>

<script>
    $("#field-estudiantes_id, #field-programacion_carreras_id").change(function(e){
        if($("#field-estudiantes_id").val()!=='' && $("#field-programacion_carreras_id").val()!==''){
                e.stopPropagation();
                var selectedValue = $('#field-estudiantes_id').val();
                var old = $('#field-matriculas_id').val();
                $.post('ajax_extension/matriculas_id/', {estudiantes_id:selectedValue,programacion_carreras_id:$("#field-programacion_carreras_id").val()}, function(data) {	
                var $el = $('#field-matriculas_id');
                var newOptions = data;
                $el.empty(); // remove old options
                $el.append($('<option></option>').attr('value', '').text(''));
                $.each(newOptions, function(key, value) {
                  $el.append($('<option></option>')
                     .attr('value', key).text(value.replace(/&(nbsp|amp|quot|lt|gt);/g,' ')));
                      if(key==old){
                          $el.val(old);
                      }
                  });
                $el.chosen().trigger('liszt:updated');
                $(".chzn-container").css('width','100%');
            },'json');
        }
    });
</script>