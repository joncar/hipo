<?php echo $output ?>
<div style="display:none" id="extrafields">
    <div id="detalles_field_box" class="form-group">
    <label id="plan_estudio_id_display_as_box" for="field-detalles_id">
            Detalles:
    </label>
        <div>
            <div class="row">
                <div class="col-xs-3"><b>Materia</b></div>
                <div class="col-xs-3"><b>Fecha Examen</b></div>
                <div class="col-xs-2"><b>Hora Inicio</b></div>
                <div class="col-xs-2"><b>Hora Fin</b></div>                
            </div>
            <?php if(!empty($detalles)): ?>
                <?php foreach($detalles->result() as $d): ?>
                    <div class="row programacionMateriasPlan">
                        <div class="col-xs-3"><?= form_dropdown_from_query('materia[]',$this->planacademico_model->getInscripcionListMaterias(),'id','materia_nombre anho_lectivo',$d->programacion_materias_plan_id,'',FALSE,'form-control materiaField','Seleccione') ?></div>
                        <div class="col-xs-3"><input name='fecha_examen[]' type="text" class="form-control fecha" value='<?= date("d-m-Y",strtotime($d->fecha)) ?>'></div>
                        <div class="col-xs-2"><input name='hora_ini[]' type="text" class="form-control hora" value='<?= $d->hora_inicio ?>'></div>
                        <div class="col-xs-2"><input name='hora_fin[]' type="text" class="form-control hora" value='<?= $d->hora_fin ?>'></div>
                        <div class="col-xs-2"><a href="#" class='add'><i class="fa fa-plus-circle"></i></a> <a href="#" class='remove' style="color:red"><i class="fa fa-remove"></i></a></div>
                    </div>
                <?php endforeach ?>
            <?php endif ?>
            <div class="row programacionMateriasPlan">
                <div class="col-xs-3"><?= form_dropdown_from_query('materia[]',$this->planacademico_model->getInscripcionListMaterias(),'id','materia_nombre anho_lectivo',0,'',FALSE,'form-control materiaField','Seleccione') ?></div>
                <div class="col-xs-3"><input name='fecha_examen[]' type="text" class="form-control fecha"></div>
                <div class="col-xs-2"><input name='hora_ini[]' type="text" class="form-control hora"></div>
                <div class="col-xs-2"><input name='hora_fin[]' type="text" class="form-control hora"></div>
                <div class="col-xs-2"><a href="#" class='add'><i class="fa fa-plus-circle"></i></a> <a href="#" class='remove' style="color:red"><i class="fa fa-remove"></i></a></div>
            </div>
        </div>
    </div>
</div>
<script src='<?= base_url('js/jquery.mask.js'); ?>'></script>
<script>
    function add(obj){
        obj = $(obj).parents('.programacionMateriasPlan');
        var element = obj.clone();
        obj.after(element);
        var index = obj.index();
        var newRow = $(".programacionMateriasPlan")[index];
        newRow = $(newRow);
        newRow.find('input[type="date"], input[type="time"]').val('');
    }
    function remove(obj){
        $(obj).parents('.programacionMateriasPlan').remove();
    }
    $(document).on('click','.remove',function(e){
        e.preventDefault();
        remove($(this));
    });
    $(document).on('click','.add',function(e){
        e.preventDefault();
        add($(this));
    });
    $(document).on('change','#field-plan_estudio_id',function(){
        if($(this).val()!=''){
            $.get('<?php echo base_url('procesosacademicos/procesosacademicosJson/refreshInscripcionListMaterias') ?>/'+$(this).val()+'/'+$("#field-anho_lectivo").val(),{},function(data){
                $(".materiaField").replaceWith(data);
            });
        }
    });
    $(document).on('ready',function(){
        $("#plan_estudio_id_field_box").after($("#extrafields").html());
        $('.programacionMateriasPlan .fecha').mask("00-00-0000", {placeholder: "__-__-____",clearIfNotMatch: true});
        $('.programacionMateriasPlan .hora').mask("00:00", {placeholder: "__:__",clearIfNotMatch: true});        
    });
</script>