<footer id="footer" class="footer--style-3">
    <div class="footer__inner">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-lg-4">
                    <div class="footer__item">
                        <a class="site-logo" href="<?= site_url() ?>">AIRFOC</a>
                        <div class="footer__text">
                            <p>
                                <strong>La principal seguretat és no preocupar-te.</strong>
                            </p>
                            <p>
                                Nosaltres ens ocupem que el teu negoci o llar estigui sempre protegida per un incendi. No cal que hi pensis, AIRFOC ens preocupem per tu.<br>
                            </p>
                        </div>
                        <p class="footer__copy">
                            © 2017, Airfoc. Tots el drets reservats.&nbsp;Fet a <a href="#" target="_blank">Hipo</a><br>
                        </p>
                    </div>
                </div>
                <div class="col-md-6 col-lg-4">
                    <div class="footer__item">
                        <h3 class="footer__title">MENU</h3>
                        <div class="row">
                            <div class="col-xs">
                                <ul class="footer__menu">
                                   <li><a href="<?= site_url() ?>#serveis">SERVEIS</a><br></li>
                                    <li><a href="<?= site_url() ?>#qui_som">qui som?</a><br></li>
                                    <li><a href="<?= site_url() ?>#efectivitat">EFECTIVITAT</a><br></li>
                                    <li><a href="<?= site_url() ?>#pressupost">PRESSUPOST</a><br></li>
                                    <li><a href="<?= site_url() ?>#productes">PRODUCTES</a><br></li>
                                    <li><a href="<?= site_url() ?>#noticies">NOTÍCIES</a><br></li>
                                </ul>
                            </div>
                            <div class="col-xs">
                                <ul class="footer__menu">
                                    <li><a href="<?= site_url('p/co2') ?>">Extintors C02</a><br></li>
                                    <li><a href="<?= site_url('p/pols') ?>">Extintors de pols</a><br></li>
                                    <li><a href="<?= site_url('p/accesoris') ?>">Accesoris</a><br></li>
                                    <li><a href="<?= site_url('p/senalitzacio') ?>">Señalització</a><br></li>
                                    <li><a href="<?= site_url('p/avis-legal') ?>">Avís legal</a><br></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 col-lg-4">
                    <div class="footer__item">
                        <h3 class="footer__title">Contacte</h3>
                        <div class="company-contacts">
                            <address>
                                <p>
                                    <i class="fontello-location"></i> c/ Francesc Moragas, 11. 08700 IGUALADA
                                </p>
                                <p>
                                    <i class="fontello-phone-call"></i>93 805 58 09 / 679 47 33 32
                                </p>
                                <p>
                                    <i class="fontello-mail"></i>
                                    <a href="mailto:support@watchland.com">info@airfoc.cat</a>
                                    <br>
                                </p>
                            </address>
                            <div class="social-btns">
                                <div class="social-btns__inner">
                                    <a class="fontello-twitter" href="#" target="_blank"></a>
                                    <a class="fontello-facebook" href="#" target="_blank"></a>
                                    <a class="fontello-linkedin-squared" href="#" target="_blank"></a>
                                    <a class="fontello-youtube" href="#" target="_blank"></a><br>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer><!-- end footer -->
<div id="btn-to-top-wrap">
    <a id="btn-to-top" class="circled mce-item-anchor" data-visible-offset="1000"></a>
    <br>
</div>