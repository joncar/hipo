<!-- Google Maps Script -->
<script src="http://maps.google.com/maps/api/js?sensor=true"></script>
<!-- Gmap3.js For Static Maps -->
<script src="<?= base_url() ?>js/template/gmap3.js"></script>
<script src="<?= base_url() ?>js/template/plugins.js"></script>
<script src="<?= base_url() ?>js/template/snap.svg-min.js"></script>
<script src="<?= base_url() ?>js/template/slick.min.js"></script>
<script src="<?= base_url() ?>js/template/main.js"></script>
<script src="<?= base_url() ?>js/template/piechart.js"></script>
<script type="text/javascript">
    /*---------------------- Retina -------------------------*/
    // Spoof the browser into thinking it is Retina
    // comment the next line out to make sure it works without retina
    window.devicePixelRatio = 2;
    $(document).ready(function () {
        $('#go-to-next').on('click', function (event) {
            event.preventDefault();
            $.scrollTo($(".about-section"), {
                axis: 'y',
                duration: 500
            });
            alert('');
            return false;
        });
    });
    function isMobile() {
        return ('ontouchstart' in document.documentElement);
    }
    function init_gmap() {
        if (typeof google == 'undefined')
            return;
        var options = {
            center: [41.592389, 1.613618],
            zoom: 15,
            /*mapTypeControl: true,
             mapTypeControlOptions: {
             //style: google.maps.MapTypeControlStyle.DROPDOWN_MENU
             },
             navigationControl: true,*/
            //scrollwheel: false,
            //streetViewControl: true
        }
        if (isMobile()) {
            options.draggable = false;
        }
        $('#googleMaps').gmap3({
            map: {
                options: options
            },
            marker: {
                latLng: [41.592389, 1.613618],
                options: {icon: 'images/mapicon.png', title: 'Hipo.tv'},
                events: {
                    click: function (marker, event, context) {
                        console.log(event);
                    }
                }
            }
        });
    }
    init_gmap();
    (function ($) {
        "use strict";
        
    })(jQuery);
</script>
<script>
    $(document).ready(function() {
        $('.testimonial-slider').slick({
            autoplay: true,
            dots: false,
            infinite: true,
            speed: 300,
            slidesToShow: 1,
            adaptiveHeight: true
        });

        /*----------------------- Circle Skill Bars ----------------------*/

        var chart = $('.chart'),
                chartNr = $('.chart-content'),
                chartParent = chart.parent();

        function centerChartsNr() {
            chartNr.css({
                top: (chart.height() - chartNr.outerHeight()) / 2
            });
        }

        if (chart.length) {
            centerChartsNr();
            $(window).resize(centerChartsNr);

            chartParent.each(function () {
                $(this).onScreen({
                    doIn: function () {
                        $(this).find('.chart').easyPieChart({
                            scaleColor: '#eeeeee',
                            lineWidth: 10,
                            size: 170,
                            trackColor: '#eeeeee',
                            lineCap: 'square',
                            animate: 5000,
                            onStep: function (from, to, percent) {
                                $(this.el).find('.percent').text(Math.round(percent));
                            }
                        });
                    },
                });
            });
        }

    });


    (function ($) {

        "use strict";
        $(document).ready(function () {

//            $(".animsition").animsition({
//                inClass: 'fade-in',
//                outClass: 'fade-out',
//                inDuration: 800,
//                outDuration: 800,
//                linkElement: '.animsition-link',
//                // e.g. linkElement   :   'a:not([target="_blank"]):not([href^=#])'
//                loading: true,
//                loadingParentElement: 'body', //animsition wrapper element
//                loadingClass: 'animsition-loading',
//                unSupportCss: ['animation-duration',
//                    '-webkit-animation-duration',
//                    '-o-animation-duration'
//                ],
//                //"unSupportCss" option allows you to disable the "animsition" in case the css property in the array is not supported by your browser. 
//                //The default setting is to disable the "animsition" in a browser that does not support "animation-duration".
//
//                overlay: false,
//                overlayClass: 'animsition-overlay-slide',
//                overlayParentElement: 'body'
//            });
        });
    })(jQuery);


</script>
