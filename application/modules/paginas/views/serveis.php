<div class="container">
    <div class="wrapper service-page"><!-- wrapper needed for scroll -->
        <div class="main clearfix">

            <div class="column">
                <p><button id="trigger-overlay" class="showMenu"><span class="ti-menu"></span></button></p>
            </div>

            <div class="overlay overlay-genie" data-steps="m 701.56545,809.01175 35.16718,0 0,19.68384 -35.16718,0 z;m 698.9986,728.03569 41.23353,0 -3.41953,77.8735 -34.98557,0 z;m 687.08153,513.78234 53.1506,0 C 738.0505,683.9161 737.86917,503.34193 737.27015,806 l -35.90067,0 c -7.82727,-276.34892 -2.06916,-72.79261 -14.28795,-292.21766 z;m 403.87105,257.94772 566.31246,2.93091 C 923.38284,513.78233 738.73561,372.23931 737.27015,806 l -35.90067,0 C 701.32034,404.49318 455.17312,480.07689 403.87105,257.94772 z;M 51.871052,165.94772 1362.1835,168.87863 C 1171.3828,653.78233 738.73561,372.23931 737.27015,806 l -35.90067,0 C 701.32034,404.49318 31.173122,513.78234 51.871052,165.94772 z;m 52,26 1364,4 c -12.8007,666.9037 -273.2644,483.78234 -322.7299,776 l -633.90062,0 C 359.32034,432.49318 -6.6979288,733.83462 52,26 z;m 0,0 1439.999975,0 0,805.99999 -1439.999975,0 z">
                <svg xmlns="http://www.w3.org/2000/svg" width="100%" height="100%" viewBox="0 0 1440 806" preserveAspectRatio="none">
                    <path class="overlay-path" d="m 701.56545,809.01175 35.16718,0 0,19.68384 -35.16718,0 z"/>
                </svg>
                <button type="button" class="overlay-close"><span class="ti-close"></span></button>

                <img class="menu-logo" src="<?= base_url() ?>images/logo-white.png" alt="Site Logo">
                <?php $this->load->view('includes/template/menu'); ?>
            </div>

            <section class="cd-section section-1">
                <div class="cd-block">
                    <div class="cd-half-block left-side">
                        <div class="our-success">
                            <h3 class="title">Our Success</h3>
                            <p>We never neglect our work. We all have honor of all. We love fun and we are also serious too. </p>
                            <ul class="success-count-container">
                                <li>
                                    <div class="success-counter">
                                        <span class="item-icon"><span class="ti-stats-up"></span></span>
                                        <span class="count-number count counter">360</span>
                                        <span class="item-name">Projects</span>
                                    </div>
                                </li>
                                <li>
                                    <div class="success-counter">
                                        <span class="item-icon"><span class="ti-user"></span></span>
                                        <span class="count-number count counter">162</span>
                                        <span class="item-name">Happy Clients</span>
                                    </div>
                                </li>
                                <li>
                                    <div class="success-counter">
                                        <span class="item-icon"><span class="ti-crown"></span></span>
                                        <span class="count-number count counter">255</span>
                                        <span class="item-name">Awards</span>
                                    </div>
                                </li>
                            </ul><!-- /.success-count-container -->
                        </div><!-- /.our-success -->

                        <div class="other-services">
                            <h3 class="title">Other Services</h3>
                            <p>One Developer, One Designer and One Marketer are all in all of our Team. We all respect our work.</p>
                            <ul class="list-items">
                                <li><span class="right-icon ti-arrow-circle-right"></span><p>Sed cursus dignissim sem, vel facilisis magna bibendum quis</p></li>
                                <li><span class="right-icon ti-arrow-circle-right"></span><p>Vestibulum et viverra arcu, vel dictum ante.</p></li>
                                <li><span class="right-icon ti-arrow-circle-right"></span><p>Quisque ut sem vel orci cursus ultricies auctor eget odio.</p></li>
                            </ul><!-- /.list-items -->
                        </div><!-- /.other-services -->
                    </div>

                    <div class="cd-half-block right-side">
                        <h2 class="section-title">Services</h2>
                        <p class="section-description">Designing, Branding, Developing and Marketing are our main service. We always provide better service to our client. </p>
                        <div class="service-details">
                            <h3 class="title">Our Services</h3>
                            <p>Class aptent taciti sociosqu ad litora torquent per conubia nostra per inceptos himenaeos. </p>
                            <div class="service-items">
                                <div class="item">
                                    <div class="icon-container">
                                        <span class="item-icon ti-ruler-pencil"></span>
                                    </div><!-- /.icon-container -->
                                    <h4 class="item-title">Designing</h4>
                                    <p class="item-details">Our Designer first scretch new design with his hand. After sketch he make PSD.</p>
                                </div><!-- /.item -->
                                <div class="item">
                                    <div class="icon-container">
                                        <span class="item-icon ti-desktop"></span>
                                    </div><!-- /.icon-container -->
                                    <h4 class="item-title">Developing</h4>
                                    <p class="item-details">We have a Branding experience person. He is experience more then 5 year.</p>
                                </div><!-- /.item -->
                                <div class="item">
                                    <div class="icon-container">
                                        <span class="item-icon ti-shopping-cart-full"></span>
                                    </div><!-- /.icon-container -->
                                    <h4 class="item-title">Marketing</h4>
                                    <p class="item-details">
                                        Website control, Prduct Research and some other always done by our Marketer. </p>
                                </div><!-- /.item -->
                                <div class="item">
                                    <div class="icon-container">
                                        <span class="item-icon ti-settings"></span>
                                    </div><!-- /.icon-container -->
                                    <h4 class="item-title">Support</h4>
                                    <p class="item-details">Our Developer mainly works with PHP, Javascript, Ruby and Python. He is really awesome.</p>
                                </div><!-- /.item -->
                            </div><!-- /.service-items -->
                        </div><!-- /.service-details -->
                    </div>
                </div> <!-- /.cd-block -->
            </section> <!-- .cd-section -->
        </div><!-- /main -->
    </div><!-- wrapper -->
</div><!-- /container -->
