<div class="container">
    <div class="wrapper about-page"><!-- wrapper needed for scroll -->
        <div class="main clearfix">

            <div class="column">
                <p><button id="trigger-overlay" class="showMenu"><span class="ti-menu"></span></button></p>
            </div>

            <div class="overlay overlay-genie" data-steps="m 701.56545,809.01175 35.16718,0 0,19.68384 -35.16718,0 z;m 698.9986,728.03569 41.23353,0 -3.41953,77.8735 -34.98557,0 z;m 687.08153,513.78234 53.1506,0 C 738.0505,683.9161 737.86917,503.34193 737.27015,806 l -35.90067,0 c -7.82727,-276.34892 -2.06916,-72.79261 -14.28795,-292.21766 z;m 403.87105,257.94772 566.31246,2.93091 C 923.38284,513.78233 738.73561,372.23931 737.27015,806 l -35.90067,0 C 701.32034,404.49318 455.17312,480.07689 403.87105,257.94772 z;M 51.871052,165.94772 1362.1835,168.87863 C 1171.3828,653.78233 738.73561,372.23931 737.27015,806 l -35.90067,0 C 701.32034,404.49318 31.173122,513.78234 51.871052,165.94772 z;m 52,26 1364,4 c -12.8007,666.9037 -273.2644,483.78234 -322.7299,776 l -633.90062,0 C 359.32034,432.49318 -6.6979288,733.83462 52,26 z;m 0,0 1439.999975,0 0,805.99999 -1439.999975,0 z">
                <svg xmlns="http://www.w3.org/2000/svg" width="100%" height="100%" viewBox="0 0 1440 806" preserveAspectRatio="none">
                    <path class="overlay-path" d="m 701.56545,809.01175 35.16718,0 0,19.68384 -35.16718,0 z"/>
                </svg>
                <button type="button" class="overlay-close"><span class="ti-close"></span></button>
                <a href="<?= site_url() ?>">
                    <img class="menu-logo" src="<?= base_url() ?>images/logo-white.png" data-rjs="3" alt="Site Logo">
                </a>
                <?php $this->load->view('includes/template/menu'); ?>
            </div>

            <section class="cd-section section-1">
                <div class="cd-block">
                    <a href="<?= site_url() ?>">
                        <img class="site-logo" src="<?= base_url() ?>images/logo.png" alt="Site Logo">
                    </a>
                    <div class="cd-half-block left-side">
                        <div class="our-team">
                            <h3 class="title">Equip</h3>
                            <p>Format per un equip amb una àmplia experiència en els sectors del disseny gràfic, direcció d'art, comunicació i desenvolupament de webs/apps.
                                A més, comptem amb proveedors que participen en moments determinats garantint l'èxit i la professionalitaten els projectes.
                                Coneix-nos.</p>
                            <div class="team-details">
                                <div class="member">
                                    <span class="member-avatar"><img src="<?= base_url() ?>images/member/1.jpg" alt="Member Avatar"></span>
                                    <div class="member-details">
                                        <h4 class="member-name">Jordi Magaña</h4>
                                        <span class="member-position">Gràfic + Web</span>
                                        <div class="member-social">
                                            <ul>
                                                <li><a class="facebook" href="#"><span class="ti-facebook"></span></a></li>
                                                <li><a class="twitter" href="#"><span class="ti-twitter"></span></a></li>
                                                <li><a class="skype" href="#"><span class="ti-skype"></span></a></li>
                                                <li><a class="linkedin" href="#"><span class="ti-linkedin"></span></a></li>
                                            </ul>
                                        </div><!-- /.member-social -->
                                    </div><!-- /.member-details -->
                                </div><!-- /.member -->
                                <div class="member">
                                    <span class="member-avatar"><img src="<?= base_url() ?>images/member/2.jpg" alt="Member Avatar"></span>
                                    <div class="member-details">
                                        <h4 class="member-name">Eliana Tusell</h4>
                                        <span class="member-position">Gràfic + Administriació</span>
                                        <div class="member-social">
                                            <ul>
                                                <li><a class="facebook" href="#"><span class="ti-facebook"></span></a></li>
                                                <li><a class="twitter" href="#"><span class="ti-twitter"></span></a></li>
                                                <li><a class="skype" href="#"><span class="ti-skype"></span></a></li>
                                                <li><a class="linkedin" href="#"><span class="ti-linkedin"></span></a></li>
                                            </ul>
                                        </div><!-- /.member-social -->
                                    </div><!-- /.member-details -->
                                </div><!-- /.member -->
                                <div class="member">
                                    <span class="member-avatar"><img src="<?= base_url() ?>images/member/3.jpg" alt="Member Avatar"></span>
                                    <div class="member-details">
                                        <h4 class="member-name">Miriam Salazar</h4>
                                        <span class="member-position">Comunicació + Xarxes socials</span>
                                        <div class="member-social">
                                            <ul>
                                                <li><a class="facebook" href="#"><span class="ti-facebook"></span></a></li>
                                                <li><a class="twitter" href="#"><span class="ti-twitter"></span></a></li>
                                                <li><a class="skype" href="#"><span class="ti-skype"></span></a></li>
                                                <li><a class="linkedin" href="#"><span class="ti-linkedin"></span></a></li>
                                            </ul>
                                        </div><!-- /.member-social -->
                                    </div><!-- /.member-details -->
                                </div><!-- /.member -->
                                <div class="member">
                                    <span class="member-avatar"><img src="<?= base_url() ?>images/member/4.jpg" alt="Member Avatar"></span>
                                    <div class="member-details">
                                        <h4 class="member-name">Jonathan Cardozo/h4>
                                            <span class="member-position">Web + Programació</span>
                                            <div class="member-social">
                                                <ul>
                                                    <li><a class="facebook" href="#"><span class="ti-facebook"></span></a></li>
                                                    <li><a class="twitter" href="#"><span class="ti-twitter"></span></a></li>
                                                    <li><a class="skype" href="#"><span class="ti-skype"></span></a></li>
                                                    <li><a class="linkedin" href="#"><span class="ti-linkedin"></span></a></li>
                                                </ul>
                                            </div><!-- /.member-social -->
                                    </div><!-- /.member-details -->
                                </div><!-- /.member -->
                            </div><!-- /.team-details -->
                        </div><!-- /.our-team -->
                    </div>

                    <div class="cd-half-block right-side">
                        <div class="our-story">
                            <h3 class="title">Estudi</h3>
                            <p>Som capaços de desenvolupar projectes de disseny a qualsevol escala, assegurant el millor en cada especialitat del disseny en la qual treballem, tot això emmarcat sota una direcció d'art molt cuidada, des de la qual controlem cadascuna de les branques de les que es componen els projectes. </p>
                        </div><!-- /.our-story -->
                        <div class="about-details">
                            <div class="item">
                                <h4 class="item-title">Filosofia</h4>
                                <p class="item-details" style=" color: #000; font-size: 0.8em; padding-top: 16px; letter-spacing: .2px; word-spacing: .4px; line-height: 20px;margin-right: 10%;">"Fem coses. A aquells que no fan mai res no els tinguem en compte, si no és per di'ls-hi això: Feu coses o calleu. No sigueu el femer que destorba al vianant. I si volen parlar, no els admetem disputa. Fem coses."
                                    <div style=" font: cursive; font-size: 0.8em; font-style: italic; font-family: open sans;">Joan Salvat-Papasseit</div></p>
                            </div><!-- /.item -->
                            <div class="item">
                                <h4 class="item-title">Misió</h4>
                                <p class="item-details" style=" color: #000; font-size: 0.8em; padding-top: 16px; letter-spacing: .2px; word-spacing: .4px; line-height: 20px;margin-right: 10%;">El principal objectiu que perseguim és contribuir a l'èxit dels teus projectes, buscant sempre l'equilibri entre la funcionalitat i l'estètica, perquè aconsegueixis una comunicació efectiva i d'acord a les teves necessitats.</p>
                            </div><!-- /.item -->
                            <div class="item">
                                <h4 class="item-title">Visió</h4>
                                <p class="item-details" style=" color: #000; font-size: 0.8em; padding-top: 16px; letter-spacing: .2px; word-spacing: .4px; line-height: 20px;margin-right: 10%;">La dedicació en cadascun dels nostres treballs, anhelem créixer constantment en el camp del disseny i la comunicació visual; aspirem que els serveis i productes que oferim siguin identificables per la seva originalitat i qualitat.</p>
                            </div><!-- /.item -->
                            <div class="item">
                                <h4 class="item-title">Misió</h4>
                                <p class="item-details" style=" color: #000; font-size: 0.8em; padding-top: 16px; letter-spacing: .2px; word-spacing: .4px; line-height: 20px;margin-right: 10%;">La comunicació, el compromís, la serietat i el respecte són els valors que ens identifiquen i els abanderats de la nostra tasca. Gaudim del nostre treball al costat de vós, compartint la passió pel disseny gràfic.</p>
                            </div><!-- /.item -->
                        </div><!-- /.about-details -->
                    </div>
                </div><!-- /#about -->
            </section> <!-- .cd-section -->

            <section class="cd-section section-2">
                <div class="cd-block">
                    <div class="cd-half-block left-side">
                        <h3 class="title">Opinions</h3>
                        <p>We respect our work. We never neglect our work. We all have honor of all. We love fun and we are also serious too. </p>

                        <div class="testimonial-slider owl-carousel">

                            <div class="item">
                                <blockquote>
                                    <span class="leftq quotes">&ldquo;</span> He promptly completed the task at hand and communicates really well till the project reaches the finishing line. I was pleased with his creative design and will definitely be hiring him again. <span class="rightq quotes">&bdquo; </span>
                                </blockquote>
                                <img src="<?= base_url() ?>images/member/1.jpg" alt="Client Image">
                                    <h2 class="quote-author">Steve Kruger</h2>
                                    <h6 class="designation">UI/UX Designer at Woof Design Studio</h6>
                            </div>

                            <div class="item">
                                <blockquote>
                                    <span class="leftq quotes">&ldquo;</span> 
                                    He promptly completed the task at hand and communicates really well till the project reaches the finishing line. I recommend him to anyone who wants their work done professionally.
                                    <span class="rightq quotes">&bdquo; </span>
                                </blockquote>
                                <img src="<?= base_url() ?>images/member/2.jpg" alt="Client Image">
                                    <h2 class="quote-author">John Doe</h2>
                                    <h6 class="designation">Developer Relations at Woof Studios</h6>
                            </div>

                            <div class="item">
                                <blockquote>
                                    <span class="quotes leftq"> &ldquo;</span> He promptly completed the task at hand and communicates really well till the project reaches the finishing line. I was pleased with his creative design and will definitely be hiring him again. <span class="rightq quotes">&bdquo; </span>
                                </blockquote>
                                <img src="<?= base_url() ?>images/member/3.jpg" alt="Client Image">
                                    <h2 class="quote-author">Steve Stevenson</h2>
                                    <h6 class="designation">CEO Woof Web Design Studios</h6>
                            </div> 
                        </div>

                    </div>

                    <div class="cd-half-block right-side">
                        <div class="our-skills">
                            <h3 class="title">Habilitats</h3>
                            <p>Designing, Branding, Developing and Marketing are our main service. We always provide better service to our client.  </p>
                            <div class="skills-details">
                                <div class="progress-wrap">
                                    <div class="progress-items circle">
                                        <div class="chart" data-percent="50" data-bar-color="#e3360d" data-animate="2000">
                                            <div class="chart-content">
                                                <div class="percent">60</div>
                                                <span>%</span>
                                            </div>
                                        </div>
                                        <h4 class="chart-title">BRANDING</h4>
                                    </div>
                                </div>
                                <div class="progress-wrap">
                                    <div class="progress-items circle">
                                        <div class="chart" data-percent="75" data-bar-color="#e3360d" data-animate="2000">
                                            <div class="chart-content">
                                                <div class="percent">90</div>
                                                <span>%</span>
                                            </div>
                                        </div>
                                        <h4 class="chart-title">DISSENY</h4>
                                    </div>
                                </div>
                                <div class="progress-wrap">
                                    <div class="progress-items circle">
                                        <div class="chart" data-percent="45" data-bar-color="#e3360d" data-animate="2000">
                                            <div class="chart-content">
                                                <div class="percent">85</div>
                                                <span>%</span>
                                            </div>
                                        </div>
                                        <h4 class="chart-title">WEB / APP</h4>
                                    </div>
                                </div>
                                <div class="progress-wrap">
                                    <div class="progress-items circle">
                                        <div class="chart" data-percent="90" data-bar-color="#e3360d" data-animate="2000">
                                            <div class="chart-content">
                                                <div class="percent">80</div>
                                                <span>%</span>
                                            </div>
                                        </div>
                                        <h4 class="chart-title">COMUNICACIÓ</h4>
                                    </div>

                                </div>

                            </div><!-- /.skill-details -->
                        </div><!-- /.our-skills -->		
                    </div>
                </div><!-- /#team -->
            </section> <!-- .cd-section -->


        </div><!-- /main -->

    </div><!-- wrapper -->
</div><!-- /container -->
<nav>
        <ul class="cd-vertical-nav">
                <li><a href="#0" class="cd-prev inactive"><span class="ti-arrow-right"></span></a></li>
                <li><a href="#0" class="cd-next"><span class="ti-arrow-right"></span></a></li>
        </ul>
</nav> <!-- .cd-vertical-nav -->
