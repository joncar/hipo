<body class="no-skin">
        <?php $this->load->view('includes/header') ?>
    <div class="main-container" id="main-container">
        <?php $this->load->view('includes/sidebar') ?>
        <div class="main-content">
            <div class="main-content-inner">
                <?php $this->load->view('includes/breadcum') ?>
                <div class="page-content">
                    <div class="page-header">
                        <h1>
                            <?= empty($title) ? 'Escritorio' : $title ?>
                            <small>
                                <i class="ace-icon fa fa-angle-double-right"></i>
                            </small>
                        </h1>
                    </div><!-- /.page-header -->

                    <div class="row">
                        <div class="col-xs-12">            
                            
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h1 class="panel-title">Lista de archivos</h1>
                                    </div>
                                    <div class="panel-body">
                                        <a href="<?= base_url('paginas/admin/paginas/add') ?>" class="btn btn-success"><i class="fa fa-plus-square"></i> Añadir</a>
                                        <table class="table table-bordered">
                                            <thead>
                                                <tr>
                                                    <th>Archivo</th><th>File</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php foreach($files as $f): ?>
                                                    <?php if(strstr($f,'.php') && $f!=='theme.php' && $f!=='read.php'): ?>
                                                        <tr>
                                                            <td><?= $f ?></td>
                                                            <td>
                                                                <a title="Mostrar" target="_new" href="<?= base_url('p/'.str_replace('.php','',$f)) ?>" style="color:black"><i class="fa fa-eye"></i></a>
                                                                <a title="Editar" href="<?= base_url('paginas/frontend/editor/'.str_replace('.php','',$f)) ?>" style="color:black"><i class="fa fa-edit"></i></a>
                                                                <a title="Eliminar" href="javascript:eliminar('<?= $f ?>')" style="color:red"><i class="fa fa-remove"></i></a>
                                                            </td>
                                                        </tr>
                                                    <?php endif ?>
                                                <?php endforeach ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <script>
                                    function eliminar(file){
                                        if(confirm('Seguro que desea eliminar esta vista?. Esta acción no tiene vuelta atras')){
                                            document.location.href="<?= base_url('paginas/admin/paginas/delete/') ?>/"+file;
                                        }
                                    }
                                </script>
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.page-content -->
            </div>
        </div><!-- /.main-content -->			
    </div><!-- /.main-container -->
    <script src="<?= base_url("js/ace.min.js") ?>"></script>
    <script src="<?= base_url("js/jquery-ui.custom.min.js") ?>"></script>	
    <script src="<?= base_url("js/ace-elements.min.js") ?>"></script>
</body>