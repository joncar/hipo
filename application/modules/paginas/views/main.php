<div class="container index-page">
	<div class="wrapper">
		<!-- wrapper needed for scroll -->
		<div class="main clearfix">
			<?php $this->load->view('includes/template/header'); ?> 
                        <section id="home-section" class="home-section cd-section">
                            <div class="cd-block">
                                <a href="<?= site_url() ?>">
                                    <img class="site-logo" src="<?= base_url() ?>images/logo.png" alt="Site Logo">
                                </a>
                                <div class="cd-intro">
                                        <h1 class="cd-headline rotate-1 main-title"><span></span><span class="cd-words-wrapper"><b class="is-visible">CREEM MARQUES</b><b>I WEB A MIDA</b><b>NO NOMÉS BONIQUES</b><b>SINÓ TAMBÉ UTILS</b><b>TOCA RENOVAR-SE</b><b>COMENCEM?</b><b>CONTACTA'NS</b></span></h1>
                                        <h4 class="sub-title">DISSENY •&nbsp;Web •&nbsp;COMUNICACIÓ</h4>
                                        <div class="next-section">
                                                <button id="go-to-next"><span class="ti-mouse"></span></button><br>
                                        </div>
                                </div>
                                <!-- cd-intro -->
                            </div>
                        </section>
			<!-- .cd-section -->
			<section id="about" class="cd-section about-section">
			<div class="cd-block">
				<div class="cd-half-block left-side">
					<br>
				</div>
				<div class="cd-half-block right-side">
					<h2 class="section-title">Estudi</h2>
					<p class="section-description">
						Hipo és un estudi creatiu de disseny gràfic especialitzat en identitat corporativa, disseny editorial, packaging, desenvolupament de webs i apps, com també en el àmbit de la comunciació i xarxes socials. 
					</p>
					<div class="about-details">
						<div class="item">
							<h4 class="item-title">ESTRATÈGIA</h4>
							<p class="item-details">
								La innovació és una de les nostres primeres. Ens apassiona la nostra feina, i és per això que estem en constant moviment, superant-a cada pas que donem i buscant la perfecció en cada projecte que realitzem.
							</p>
						</div>
						<!-- /.item -->
						<a class="btn animsition-link" href="<?= site_url('p/nosotros') ?>">veure &nbsp;+</a>
					</div>
					<!-- /.about-details -->
				</div>
			</div>
			<!-- /#about -->
			</section>
			<!-- .cd-section -->
			<section id="service" class="cd-section service-section">
			<div class="cd-block">
				<div class="cd-half-block left-side">
					<br>
				</div>
				<div class="cd-half-block right-side">
					<h2 class="section-title">Serveis</h2>
					<p class="section-description">
						Donem servei a múltiples àrees del disseny i la comunicació, podent realitzar projectes integrals per a cada client.
					</p>
					<div class="service-details">
						<div class="item">
							<div class="icon-container">
								<span class="item-icon ti-light-bulb"></span><br>
							</div>
							<!-- /.icon-container -->
							<h4 class="item-title">Branding</h4>
							<p class="item-details">
								Com a base fonamental de la creació d'una imatge de marca.
							</p>
						</div>
						<!-- /.item -->
						<div class="item">
							<div class="icon-container">
								<span class="item-icon ti-ruler-pencil"></span><br>
							</div>
							<!-- /.icon-container -->
							<h4 class="item-title">Editorial</h4>
							<p class="item-details">
								Des d'un flyer o un catàleg fins a un llibre o una revista.
							</p>
						</div>
						<div class="service-details">
							<div class="item">
								<div class="icon-container">
									<span class="item-icon ti-package"></span><br>
								</div>
								<!-- /.icon-container -->
								<h4 class="item-title">Packaging</h4>
								<p class="item-details">
									Dissenyar un envàs, etiqueta o qualsevol tipus d'embolcall.
								</p>
							</div>
							<!-- /.item -->
							<div class="item">
								<div class="icon-container">
									<span class="item-icon ti-palette"></span><br>
								</div>
								<!-- /.icon-container -->
								<h4 class="item-title">Ilustració</h4>
								<p class="item-details">
									Un dels recursos, per enriquir qualsevol del nostre treballs.
								</p>
							</div>
							<!-- /.item -->
							<div class="service-details">
								<div class="item">
									<div class="icon-container">
										<span class="item-icon ti-mobile"></span><br>
									</div>
									<!-- /.icon-container -->
									<h4 class="item-title">Web, Apps &amp; Soft</h4>
									<p class="item-details">
										Webs i programes fets a mida que compleixi les teves necessitats
									</p>
								</div>
								<!-- /.item -->
								<div class="item">
									<div class="icon-container">
										<span class="item-icon ti-video-camera"></span><br>
									</div>
									<!-- /.icon-container -->
									<h4 class="item-title">Video</h4>
									<p class="item-details">
										Confiem en la imatge com a eina principal del disseny i la comunicació.
									</p>
								</div>
								<div class="service-details">
									<div class="item">
										<div class="icon-container">
											<span class="item-icon ti-announcement"></span><br>
										</div>
										<!-- /.icon-container -->
										<h4 class="item-title">Comunicació</h4>
										<p class="item-details">
											Estratègia comunicativa basant-nos en una anàlisi exhaustiva de l'entorn.
										</p>
									</div>
									<!-- /.item -->
									<div class="item">
										<div class="icon-container">
											<span class="item-icon ti-comments-smiley"></span><br>
										</div>
										<!-- /.icon-container -->
										<h4 class="item-title">Events</h4>
										<p class="item-details">
											Confiem en la imatge com a eina principal del disseny i la comunicació.
										</p>
									</div>
									<a class="btn animsition-link" href="<?= site_url('p/serveis') ?>">+ info</a>
								</div>
								<!-- /.service-details -->
							</div>
						</div>
						<!-- /.cd-block -->
					</div>
				</div>
			</div>
			</section>
			<!-- /#service -->
			<section id="team" class="cd-section team-section">
			<div class="cd-block">
				<div class="cd-half-block left-side">
					<br>
				</div>
				<div class="cd-half-block right-side">
					<h2 class="section-title">Equip</h2>
					<p class="section-description">
						Respectem i mai descuidem el nostre treball. Ens encanta la diversió. Ens encanta la nostra feina.
					</p>
					<div class="team-details">
						<div class="member">
  									<span class="member-avatar"><img src="images/member/2.jpg" alt="Member Avatar"></span>
  									<h4 class="member-name">Jordi Magaña</h4>
  									<span class="member-position">Gràfic + Web</span>
  								</div>
						<!-- /.member -->
						<div class="member">
  									<span class="member-avatar"><img src="images/member/2.jpg" alt="Member Avatar"></span>
  									<h4 class="member-name">Eliana Tusell</h4>
  									<span class="member-position">Gràfic + Administriació</span>
  								</div>
						<!-- /.member -->
					</div>
					<div class="team-details">
						<div class="member">
  									<span class="member-avatar"><img src="images/member/2.jpg" alt="Member Avatar"></span>
  									<h4 class="member-name">Miriam Salazar</h4>
  									<span class="member-position">Comunicació + Xarxes socials</span>
  								</div>
						<!-- /.member -->
						<div class="member">
  									<span class="member-avatar"><img src="images/member/2.jpg" alt="Member Avatar"></span>
  									<h4 class="member-name">Jonathan Cardozo</h4>
  									<span class="member-position">Web + Programació</span>
  								</div>
						<!-- /.member -->
					</div>
					
					<!-- /.about-details -->
				</div>
			</div>
			<!-- /#team -->
			</section>
			<!-- .cd-section -->
			<section id="portfolio" class="cd-section portfolio-section">
			<div class="cd-block">
				<div class="cd-half-block left-side">
					<br>
				</div>
				<div class="cd-half-block right-side">
					<h2 class="section-title">Projectes</h2>
					<p class="section-description">
						Darrere de tots nostres projectes sempre hi una estudi minucios que es converteix en un element diferenciador per els nostres clients respecte a la seva competència.
					</p>
					<div class="portfolio-details">
						<figure class="item col-md-6"><img src="images/portfolio/1.jpg" alt="Portfolio Image"><figcaption>
						<h4 class="item-title">Project Title</h4>
						<span class="member-position">WordPress</span></figcaption></figure>
						<!-- /.item -->
						<figure class="item col-md-6"><img src="images/portfolio/2.jpg" alt="Portfolio Image"><figcaption>
						<h4 class="item-title">Project Title</h4>
						<span class="member-position">Joomla</span></figcaption></figure>
						<!-- /.item -->
						<figure class="item col-md-6"><img src="images/portfolio/1.jpg" alt="Portfolio Image"><figcaption>
						<h4 class="item-title">Project Title</h4>
						<span class="member-position">WordPress</span></figcaption></figure>
						<!-- /.item -->
						<figure class="item col-md-6"><img src="images/portfolio/1.jpg" alt="Portfolio Image"><figcaption>
						<h4 class="item-title">Project Title</h4>
						<span class="member-position">WordPress</span></figcaption></figure>
						<!-- /.item -->
						<a class="btn animsition-link" href="<?= site_url('p/portafolio') ?>">Veure portafoli</a>
					</div>
					<!-- /.about-details -->
				</div>
			</div>
			<!-- /#portfolio -->
			</section>
			<!-- .cd-section -->
			<section id="blog" class="cd-section blog-section">
			<div class="cd-block">
				<div class="cd-half-block left-side">
					<br>
				</div>
				<div class="cd-half-block right-side">
					<h2 class="section-title">Blog</h2>
					<p class="section-description">
						Eines i Recursos de disseny web, disseny gràfic, xarxes socials i molt més
					</p>
					<div class="blog-details">
                                            <?php foreach($blog->result() as $detail): ?>
						<article class="post type-post">
                                                    <div class="post-meta">
                                                            <div class="entry-meta">
                                                                <div class="entry-date">                                                                        
                                                                    <time datetime="<?= $detail->fecha ?>"><span><?= date("d",strtotime($detail->fecha)) ?></span> <?= date("M",strtotime($detail->fecha)) ?></time>
                                                                </div>
                                                            </div>
                                                            <!-- /.entry-meta -->
                                                    </div>
                                                    <!-- /.post-meta -->
                                                    <div class="post-content">
                                                        <h2 class="entry-title">
                                                            <a href="<?= site_url('blog/'.toURL($detail->id.'-'.$detail->titulo)) ?>">
                                                            <?= $detail->titulo ?></a>
                                                        </h2>
                                                        <p class="entry-text">
                                                            <?= substr($detail->texto,0,255) ?>
                                                        </p>
                                                    </div>
						<!-- /.post-content -->
						</article>
                                             <?php endforeach ?>
					</div>
					<!-- /.about-details -->
				</div>
			</div>
			<!-- /#blog -->
			</section>
			<!-- .cd-section -->
			<section id="contact" class="cd-section contact-section">
			<div class="cd-block">
				<div class="cd-half-block left-side">
					<div id="google-map" class="google-map">
						<div class="map-container">
							<div id="googleMaps" class="google-map-container">
								<br>
							</div>
						</div>
						<!-- /.map-container -->
					</div>
					<!-- /#google-map -->
				</div>
				<div class="cd-half-block right-side">
					<h2 class="section-title">Contacte</h2>
					<p class="section-description">
						Si vols contractar qualsevol dels nostres serveis, demanar pressupost o preguntar. Recorda que estem per ajudar-te. Fem pressupostos sense compromís i ens adaptem a les teves necessitats.
					</p>
					<div class="contact-details">
						<div id="contact-form" class="contact-form">
							<form action="#" method="post" id="contactform" class="contactform">
								<input id="author" class="" name="author" type="text" placeholder="Nom*" value="" required="">
								<input id="email" class=" pull-right" name="email" type="email" placeholder="Email*" value="" required="">
								<input id="subject" class=" pull-right" name="subject" type="text" placeholder="Tema*" value="" required="">
								<textarea id="message" class="" name="message" placeholder="Missatge" rows="3" required=""></textarea>
								<button name="submit" class="submit-btn" type="submit" id="submit">
								<span class="ti-shift-right"></span>Enviar</button>
							</form>
							<!-- /#contactform -->
						</div>
						<!-- /#leave-contact -->
					</div>
					<!-- /.contact-details -->
					<div class="address-details">
						<div class="item">
							<div class="contact-icon">
								<span class="ti-direction-alt"></span><br>
							</div>
							<!-- /.contact-icon -->
							<address>
							<p class="street-address">
								President Lluis Companys, 28. 08700 IGUALADA. Barcelona. CATALUNYA
							</p>
							</address>
						</div>
						<div class="item">
							<div class="contact-icon">
								<span class="ti-mobile"></span><br>
							</div>
							<!-- /.contact-icon -->
							<div class="mobile">
								<span class="mobile-no"><a href="tel:+34 938 053 738">+34 938 053 738</a><br></span><a href="tel:+34 609 306 713">+34 609 306 713</a>
							</div>
							<!-- /.mobile -->
						</div>
						<div class="item">
							<ul class="social-list">
								<li><a class="facebook" href="#"><i class="ti-facebook"></i></a></li>
								<li><a class="twitter" href="#"><i class="ti-twitter"></i></a></li>
								<li><a class="linkedin" href="#"><i class="ti-linkedin"></i></a></li>
								<li><a class="dribbble" href="#"><i class="ti-dribbble"></i></a></li>
							</ul>
							<!-- /.social-list -->
						</div>
					</div>
					<!-- /.address-details -->
					<footer class="contact-bottom">
					<div class="copy-right">
                                            © <a href="<?= site_url() ?>">Hipo</a> 2017 - Tots els drets reservats.
					</div>
					<div class="page-link">
						<a href="#home-section">Inici</a>
                                                <a href="#about">Estudi</a>
                                                <a href="#blog">Blog</a>
						<!-- /.pagination -->
					</div>
					<!-- /.page-link -->
					</footer>
					<!-- /.contact-bottom -->
				</div>
			</div>
			</section>
			<!-- .cd-section -->
		</div>
		<!-- /main -->
	</div>
	<!-- wrapper -->
</div>
<!-- /container -->
<nav>
<ul class="cd-vertical-nav">
	<li><a href="#0" class="cd-prev inactive"><span class="ti-arrow-right"></span></a></li>
	<li><a href="#0" class="cd-next"><span class="ti-arrow-right"></span></a></li>
</ul>
</nav>
<!-- .cd-vertical-nav -->
