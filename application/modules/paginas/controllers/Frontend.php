<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Frontend extends Main{
        function __construct() {
            parent::__construct();
            $this->load->model('querys');
            $this->load->library('form_validation');
        }        
        
        function read($url){
            $params = $this->uri->segments;
            $this->load->model('querys');
            $this->loadView(
                    array(
                        'view'=>'read',
                        'page'=>$this->load->view($url,array(),TRUE),
                        'title'=>ucfirst(str_replace('-',' ',$url)))
            );
        }
        
        function getFormReg($x = '2'){                    
            return $this->querys->getFormReg($x);
        }
        
        function editor($url){            
            $this->load->helper('string');
            if(!empty($_SESSION['user']) && $this->user->admin==1){                
                $page = file_get_contents('application/modules/paginas/views/'.$url.'.php');
                $page = str_replace('<?php','[?php',$page);
                $page = str_replace('<?=','[?=',$page);
                $page = str_replace('&gt;','>',$page);
                $page = str_replace('&lt;','<',$page);
                $this->loadView(array('view'=>'cms/edit','scripts'=>true,'name'=>$url,'edit'=>TRUE,'page'=>$page,'title'=>'Editar '.ucfirst(str_replace('-',' ',$url))));
            }else{
                redirect(base_url());
            }
        }
        
        function contacto(){
            $this->form_validation->set_rules('email','Email','required|valid_email');
            $this->form_validation->set_rules('nombre','Nombre','required');
            $this->form_validation->set_rules('comentario','Comentario','required');
            if($this->form_validation->run()){
                $this->enviarcorreo((object)$_POST,1,'info@airfoc.cat');
                echo $this->success('Gracias por contactarnos, en breve le llamaremos');
            }else{
                echo $this->error('Por favor complete los datos solicitados  <script>$("#guardar").attr("disabled",false); </script>');
            }
        }
        
        function subscribir(){
            $this->form_validation->set_rules('email','Email','required|valid_email');
            if($this->form_validation->run()){
                $emails = $this->db->get_where('subscritos',array('email'=>$_POST['email']));
                $success = $emails->num_rows()==0?TRUE:FALSE;
                if($success){
                    $this->db->insert('subscritos',array('email'=>$_POST['email']));    
                    echo $this->success('Subscrito satisfactoriamente');
                }else{
                    echo $this->error('Correo ya existente');
                }
            }else{
                echo $this->error($this->form_validation->error_string());
            }
        }
        
        function unsubscribe(){
            if(empty($_POST)){
                $this->loadView('includes/template/unsubscribe');
            }else{
                $emails = $this->db->get_where('subscritos',array('email'=>$_POST['email']));
                $success = $emails->num_rows()>0?TRUE:FALSE;
                if($success){
                    $this->db->delete('subscritos',array('email'=>$_POST['email']));
                    echo $this->success('Correo desafiliado al sistema de noticias');
                }            
                $this->loadView(array('view'=>'includes/template/unsubscribe','success'=>$success));
            }
        }
    }