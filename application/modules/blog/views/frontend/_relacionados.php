<aside class="widget widget_recent_entries">
    <h3 class="widget-title">
        Popular Posts
    </h3><!-- /.widget-title -->
    <ul class="latest-post">
        <?php foreach($relacionados->result() as $r): ?>
            <li>
                <div class="recent-post-details">
                    <a class="post-title" href="<?= site_url('blog/'.toURL($r->id.'-'.$r->titulo)) ?>"><?= $r->titulo ?></a><br>
                    <div class="post-meta">
                        <time datetime="<?= $r->fecha ?>"><?= date("d M",strtotime($r->fecha)) ?></time> 
                        <a href="<?= site_url('blog/'.toURL($r->id.'-'.$r->titulo)) ?>"><?= $r->user ?></a>
                    </div><!-- /.post-meta -->
                </div><!-- /.recent-post-details -->
            </li>
        <?php endforeach ?>
    </ul><!-- /.latest-post -->
</aside><!-- /.widget -->