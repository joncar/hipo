<div class="container animsition">
    <div class="wrapper single-page"><!-- wrapper needed for scroll -->
        <div class="main clearfix">
            <?php $this->load->view('includes/template/header'); ?>
            <div class="blog-col-left">
                <section>
                    <div class="cd-block">
                        <a href="<?= site_url() ?>"><img class="site-logo" src="<?= base_url() ?>images/logo.png" alt="Site Logo"></a>
                    </div>
                </section>
                <div class="post-container" style="margin-top:80px;">
                    <section>
                        <h2 class="section-title">BLOG</h2>
                        <p class="section-description">
                                Donem servei a múltiples àrees del disseny i la comunicació, podent realitzar projectes integrals per a cada client.
                        </p>
                        <div class="blog-single-details">
                            <?php foreach($detail->result() as $d): ?>
                                <article class="post type-post">                            
                                    <div class="post-meta">
                                        <div class="entry-meta">
                                            <div class="entry-date">
                                                <time datetime="<?= $d->fecha ?>"><span><?= date("d",strtotime($d->fecha)) ?></span> <?= date("M",strtotime($d->fecha)) ?></time>
                                            </div>
                                        </div><!-- /.entry-meta -->
                                    </div><!-- /.post-meta -->
                                    <div class="post-content">
                                        <h2 class="entry-title"><a href="<?= site_url('blog/'.toURL($d->id.'-'.$d->titulo)) ?>"><?= $d->titulo ?></a></h2>
                                        <?= substr($d->texto,0,255) ?>
                                    </div><!-- /.post-content -->                           
                                </article><!-- /.post -->
                            <?php endforeach ?>
                        </div><!-- /.blog-single-details -->
                    </section> <!-- /#blog-single -->                    
                </div>
            </div>
            <div class="blog-col-right">
                
            </div>
        </div><!-- /main -->
    </div><!-- wrapper -->
</div><!-- /container -->