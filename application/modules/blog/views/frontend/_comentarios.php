<h3 class="title">
    <span class="title-icon ti-comments"></span><?= $comentarios->num_rows() ?> Comments
</h3>
<div class="comments">
    <ul class="comment-list">        
        <?php foreach($comentarios->result() as $c): ?>
            <li class="parent" style="width:100%">
                <article class="comment">
                    <div class="comment-author">
                        <img src="<?= base_url('assets/grocery_crud/css/jquery_plugins/cropper/vacio.png') ?>" alt="Comment Author">
                    </div>
                    <div class="comment-content">
                        <h4 class="author-name"><?= $c->autor ?></h4>
                        <span class="comment-date">
                            <span class="entry-date">
                                <time datetime="<?= date("Y-m-d",strtotime($c->fecha))."T00:00:00+00:00"; ?>"><?= date("D, d M Y",strtotime($c->fecha)); ?></time> 
                            </span>
                        </span><!-- /.comment-date -->
                        <p><?= $c->texto ?></p>
                        <a class="btn" href="#"><span class="btn-icon ti-share"></span>Reply</a>
                    </div>
                </article><!-- /.comment -->            
            </li>
         <?php endforeach ?>         
    </ul>
</div><!-- /.comments -->