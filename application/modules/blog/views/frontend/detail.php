<div class="container animsition">
    <div class="wrapper single-page"><!-- wrapper needed for scroll -->
        <div class="main clearfix">
            <?php $this->load->view('includes/template/header'); ?>
            <div class="post-container" style="margin-top:80px;">
                <section>
                    <div class="cd-block">
                        <a href="<?= site_url() ?>"><img class="site-logo" src="<?= base_url() ?>images/logo.png" alt="Site Logo"></a>
                    </div>
                </section>
                <section id="blog-single" class="cd-section blog-single">
                    <div class="blog-single-details">
                        <article class="post type-post">
                            <figure class="post-thumbnail">
                                <img src="<?= $detail->foto ?>" alt="post Image">
                            </figure><!-- /.post-thumbnail -->
                            <div class="post-meta">
                                <div class="entry-meta">
                                    <div class="entry-date">
                                        <time datetime="<?= $detail->fecha ?>"><span><?= date("d",strtotime($detail->fecha)) ?></span> <?= date("M",strtotime($detail->fecha)) ?></time>
                                    </div>
                                </div><!-- /.entry-meta -->
                            </div><!-- /.post-meta -->
                            <div class="post-content">
                                <h2 class="entry-title"><?= $detail->titulo ?></h2>
                                <?= $detail->texto ?>
                            </div><!-- /.post-content -->

                            <div class="post-bottom">
                                <div class="entry-meta">
                                    <ul>
                                        <li class="post-author">
                                            <div class="author-name">
                                                <span>cognom: <a href="#"><?= $detail->user ?></a></span>
                                            </div>
                                        </li><!-- /.post-author -->
                                        <li class="tags">
                                            <div class='tag-cataegories'> 
                                                <span>Tags:</span> 
                                                <?php foreach(explode(',',$detail->tags) as $t): ?>
                                                    <a href="#"><?= $t ?></a>,
                                                <?php endforeach ?>
                                            </div>
                                        </li><!-- /.tags -->
                                    </ul>
                                </div><!-- /.entry-meta -->		
                            </div><!-- /.post-bottom -->
                        </article><!-- /.post -->

                        <div id="post-comments" class="post-comments">
                            <div class="comment-area">
                                <?php $this->load->view('_comentarios'); ?>
                                <?php $this->load->view('_sendComentario'); ?>
                            </div><!-- /.comment-area -->
                        </div><!-- /#post-comments -->
                    </div><!-- /.blog-single-details -->
                </section> <!-- /#blog-single -->

                <div id="sidebar" class="sidebar">
                    <div class="sidebar-content">
                        <aside class="widget widget_search">
                            <form role="search" class="search-form" action="<?= site_url('blog') ?>" method="get">
                                <input class="search-field" type="text" name="direccion" id="s" placeholder="Buscar aqui" required>
                                <button class="btn search-btn" type="submit">
                                    <span class="ti-search"></span>
                                </button>
                            </form><!-- /.search-form -->
                        </aside><!-- /.widget -->

                        <aside class="widget widget_categories">
                            <h3 class="widget-title">
                                Blog Categories
                            </h3><!-- /.widget-title -->

                            <ul class="category-list">
                                <?php foreach($categorias->result() as $c): ?>
                                    <li><a href="<?= site_url('blog') ?>?blog_categorias_id=<?= $c->id ?>"><?= $c->blog_categorias_nombre ?></a></li>
                                <?php endforeach ?>
                            </ul>
                        </aside><!-- /.widget -->
                        <?php $this->load->view('_relacionados'); ?>
                        <aside class="widget widget_tagcloud clearfix">
                            <h3 class="widget-title">
                                Tag Clouds
                            </h3><!-- /.widget-title -->
                            <div class="tag-cloud-wrapper">
                                <?php foreach(explode(',',$detail->tags) as $t): ?>
                                    <a href="#"><?= $t ?></a>
                                <?php endforeach ?>
                            </div><!-- /.tag-cloud-wrapper -->
                        </aside><!-- /.widget -->
                        
                    </div><!-- /.sidebar-content -->
                </div><!-- /#sidebar -->
            </div><!-- /.post-container -->
        </div><!-- /main -->
    </div><!-- wrapper -->
</div><!-- /container -->
<script>
    $(document).ready(function () {
            $(".animsition").animsition({
                inClass: 'fade-in',
                outClass: 'fade-out',
                inDuration: 800,
                outDuration: 800,
                linkElement: '.animsition-link',
                // e.g. linkElement   :   'a:not([target="_blank"]):not([href^=#])'
                loading: true,
                loadingParentElement: 'body', //animsition wrapper element
                loadingClass: 'animsition-loading',
                unSupportCss: ['animation-duration',
                    '-webkit-animation-duration',
                    '-o-animation-duration'
                ],
                //"unSupportCss" option allows you to disable the "animsition" in case the css property in the array is not supported by your browser. 
                //The default setting is to disable the "animsition" in a browser that does not support "animation-duration".

                overlay: false,
                overlayClass: 'animsition-overlay-slide',
                overlayParentElement: 'body'
            });
        });
</script>
