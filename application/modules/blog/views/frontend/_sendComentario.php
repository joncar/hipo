<div id="leave-comment" class="clearfix leave-comment">
    <h3 class="title">
        <span class="title-icon ti-comment-alt"></span>Deja tu comentario
    </h3>
    <div id="respond">
        <form action="<?= base_url('blog/frontend/comentarios') ?>" method="post" id="commentform" class="commentform">        
            <input id="author" class="name" name="autor" type="text" placeholder="Name*" value="" required>
            <input id="email" class="email" name="email" type="email" placeholder="Email*" value="" required>
            <input id="text" class="url" name="titulo" type="text" placeholder="Titulo*" value="" required>
            <input name="blog_id" value="<?= $detail->id ?>" type="hidden">            
            <textarea id="comment" class="comment" name="texto" placeholder="Comentario" rows="1" required></textarea>
            <button class="submit-btn" type="submit" id="submit">
                <span class="ti-shift-right"></span>Submit
            </button>
        </form><!-- /#commentform -->
        <?php if(!empty($_SESSION['mensaje'])){
           echo $_SESSION['mensaje'];
           unset($_SESSION['mensaje']);
        }?>
    </div><!-- /#respond -->
</div><!-- /#leave-comment -->